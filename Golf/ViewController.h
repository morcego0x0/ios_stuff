//
//  ViewController.h
//  Golf
//
//  Created by Admin on 3/19/16.
//  Copyright © 2016 in-house dev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *lastGameButton;

@property (weak, nonatomic) IBOutlet UIButton *editFriendsButton;

@property (weak, nonatomic) IBOutlet UIButton *editCategoriesButton;

@property (weak, nonatomic) IBOutlet UIButton *nGameButton;

@end

