//
//  main.m
//  Golf
//
//  Created by Admin on 3/19/16.
//  Copyright © 2016 in-house dev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
