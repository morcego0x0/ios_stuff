//
//  ViewController.m
//  Golf
//
//  Created by Admin on 3/19/16.
//  Copyright © 2016 in-house dev. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _lastGameButton.layer.borderWidth = 1;
    _lastGameButton.layer.borderColor = [[UIColor blueColor]CGColor];
    
    _editFriendsButton.layer.borderWidth = 1;
    _editFriendsButton.layer.borderColor = [[UIColor blueColor]CGColor];
    
    _editCategoriesButton.layer.borderWidth = 1;
    _editCategoriesButton.layer.borderColor = [[UIColor blueColor]CGColor];
    
    _nGameButton.layer.borderWidth = 1;
    _nGameButton.layer.borderColor = [[UIColor blueColor]CGColor];
    _nGameButton.layer.backgroundColor = [[UIColor greenColor]CGColor];
    
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)lastGamebutton:(id)sender {
}
- (IBAction)editFriendsButton:(id)sender {
}
- (IBAction)editCategoriesButton:(id)sender {
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
